package com.amapv2.cn.apis;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class ServiceBootReceiver extends BroadcastReceiver{

	@Override
	public void onReceive(Context context, Intent intent) {
		Intent myIntent = new Intent();  
		         myIntent.setClass(context,LocService.class);
		       context.startService(myIntent);  
	}

}
